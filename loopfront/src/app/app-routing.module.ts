import { PrincipalHomeComponent } from './principal/principal-home/principal-home.component';
import { SidebarComponent } from './template/sidebar/sidebar.component';
import { NavbarComponent } from './template/navbar/navbar.component';
import { ClientesListaComponent } from './clientes/clientes-lista/clientes-lista.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component'
import { AuthGuardiaService } from './resources/services/auth-guardia.service';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'deshboard',  component:LayoutComponent, children:[
   { path: 'home', component: HomeComponent },
  // { path: 'clientes', component: ClientesListaComponent},
  // { path: 'navbar', component: NavbarComponent },
  // { path: 'sidebar', component: SidebarComponent },
  // { path: 'principal', component: PrincipalHomeComponent}
  ],canActivate: [AuthGuardiaService],
  
},
  //{path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
