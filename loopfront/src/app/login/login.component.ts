import { LoginService } from './../resources/services/login.service';
import { AlertService } from '../resources/services/alert.service';
import { RequestLogin } from './../resources/models/RequestLogin';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../resources/services/auth.service';
import { RegisterService } from './../resources/services/register.service';
import {User} from './user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  requestLogin: RequestLogin;
  formGroup: FormGroup;
  username: string;
  email: string;
  password: string;
  password_confirmation: string;
  cadastrando: boolean;
  mensagemSucesso: string;
  errors: String[];

  constructor(
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.requestLogin = new RequestLogin();
    this.initForm();
  }

  loginProces(){
    if(this.formGroup.valid){
      this.loginService
      .login(this.formGroup.value)
      .subscribe((result: any)=>{
        this.alertService.info('Login efetuado com sucesso');
        localStorage.setItem('token', result.token);
        sessionStorage.setItem('token', result.token);
        this.router.navigate(['deshboard']);
       // this.mensagemSucesso = "login teste";
      }, (httpError) => {
        this.alertService.error(httpError.error.message);
      } );
      
    }
    //this.router.navigate(['/home']);
  }
  preparaCadastrar(event){
    event.preventDefault();
    this.cadastrando = true;
  }
  cancelaCadastro(){
    this.cadastrando = false;
  }
  emailCadastro(){
    this.cadastrando = true;
  }
  confirmPassword(){
    this.cadastrando = true;
  }
  cadastrar(){
    const user: User = new User();
    user.username = this.username;
    user.email = this.email;
    user.password = this.password;
    user.password_confirmation = this.password_confirmation;
    this.loginService
    .salvar(user)
    .subscribe( response => {
      this.mensagemSucesso = "Cadastro realizado com sucesso! Efetue o login";
      this.cadastrando = false;
      this.username = '';
      this.email = '';
      this.password = '';
      this.password_confirmation = '';
    }, errorResponse => {
      this.mensagemSucesso = null;
      this.errors = errorResponse.error.errors;
    })
  }
  initForm(){
    this.formGroup = new FormGroup({
      username: new FormControl('',[Validators.required]),
      password: new FormControl('',[Validators.required])
    });
  }
}
