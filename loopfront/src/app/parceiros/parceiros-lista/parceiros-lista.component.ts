import { ParceirosService } from './../../resources/services/parceiros.service';
import { AlertService } from './../../resources/services/alert.service';
import { Router } from '@angular/router';
import { CadastrosService } from './../../resources/services/cadastros.service';
import { Parceiro } from './../parceiros';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parceiros-lista',
  templateUrl: './parceiros-lista.component.html',
  styleUrls: ['./parceiros-lista.component.css']
})
export class ParceirosListaComponent implements OnInit {

  parceiros: Parceiro[] = [];
  parceiroSelecionado: Parceiro;
  mensagemSucesso: string;
  mensagemErro: string;

  constructor(
    private service: ParceirosService, 
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.service
    .getParceiros()
    .subscribe
    (response => this.parceiros = response);
  }
  novoCadastro(){
    this.router.navigate(['/parceiros/form'])
  }
  preparaDelecao(parceiro: Parceiro){
    this.parceiroSelecionado = parceiro;
  }
  deletarParceiro(){
    this.service
    .deletar(this.parceiroSelecionado)
    .subscribe( response => {
      this.mensagemSucesso = 'Parceiro deletado com sucesso!'
      this.ngOnInit();
    },
    erro => this.mensagemErro = 'Ocorreu um erro ao deletar o parceiro!')
  }

}
