import { FormGroup, FormControl } from '@angular/forms';
import { ParceirosService } from './../../resources/services/parceiros.service';
import { Observable } from 'rxjs';
import { AlertService } from './../../resources/services/alert.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Parceiro } from './../parceiros';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parceiros-form',
  templateUrl: './parceiros-form.component.html',
  styleUrls: ['./parceiros-form.component.css']
})
export class ParceirosFormComponent implements OnInit {
  

  parceiro: Parceiro;
  success: boolean = false;
  errors: String[];
  id: number;

  constructor(
    private service: ParceirosService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpClient: HttpClient,
    private alertService: AlertService
  ) { 
    this.parceiro = new Parceiro();
  }

  ngOnInit(): void {
    let params : Observable<any> = this.activatedRoute.params
     params.subscribe( urlParams => {
       this.id = urlParams['id'];
       if(this.id){
         this.service
         .getParceiroById(this.id)
         .subscribe(    
           response => this.parceiro = response,
           errorResponse => this.parceiro = new Parceiro() 
         )
       }
     })
  }
  voltarParaListagem(){
    this.router.navigate(['parceiros/lista']);
  }
  onSubmit(){
    if(this.id){
      this.service
      .atualizar(this.parceiro)
      .subscribe(response => {
        this.alertService.info('Parceiro atualizado com sucesso');
        this.errors = null;
      }, errorResponse => {
        this.errors['Erro ao atualizar o parceiro!']
      })

    }else {
    this.service
      .salvar(this.parceiro)
      .subscribe( response => {
        this.success = true;
        this.errors = null;
      }, errorResponse => {
        this.success = false;
        this.errors = errorResponse.error.errors;
        })
    }
  }
}
