import { ParceirosListaComponent } from './parceiros-lista/parceiros-lista.component';
import { LayoutComponent } from './../layout/layout.component';
import { ParceirosFormComponent } from './parceiros-form/parceiros-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: 'parceiros', component: LayoutComponent, children: [
    {path: 'form', component: ParceirosFormComponent},
    {path: 'form/:id', component: ParceirosFormComponent},
    {path: 'lista', component: ParceirosListaComponent},
    //assim como no de clientes, colocado por enquanto apenas.
    {path: '', redirectTo: '/parceiros/lista', pathMatch: 'full'}
    //precisa de chaves.
    //path: '', redirecTo: '/parceiros/lista', pathMatch: 'full'
  ]}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParceirosRoutingModule { }
