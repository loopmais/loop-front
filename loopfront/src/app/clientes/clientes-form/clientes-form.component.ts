import { AlertService } from './../../resources/services/alert.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cliente } from './../cliente';
import { CadastrosService } from '../../resources/services/cadastros.service';

@Component({
  selector: 'app-clientes-form',
  templateUrl: './clientes-form.component.html',
  styleUrls: ['./clientes-form.component.css']
})
export class ClientesFormComponent implements OnInit {

  cliente: Cliente;
  success: boolean = false;
  errors: String[];
  id: number;

  constructor(
    private service: CadastrosService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpClient: HttpClient,
    private alertService: AlertService
    ) { 
    this.cliente = new Cliente();
  }

  ngOnInit(): void {
    // const headers = new HttpHeaders({
    //   'Authorization': `Bearer ${localStorage.getItem('token')}`
    // })
    // this.httpClient.get('http://localhost:8000/users', {headers: headers}).subscribe(
    //   result => this.user = result
    // )
     let params : Observable<any> = this.activatedRoute.params
     params.subscribe( urlParams => {
       this.id = urlParams['id'];
       if(this.id){
         this.service
         .getClienteById(this.id)
         .subscribe(    
           response => this.cliente = response,
           errorResponse => this.cliente = new Cliente() 
         )
       }
     }) 
  }
  voltarParaListagem(){
    this.router.navigate(['clientes/lista']);
  }
  onSubmit(){
    if(this.id){
      this.service
      .atualizar(this.cliente)
      .subscribe(response => {
        this.alertService.info('Cliente atualizado com sucesso');
        this.errors = null;
      }, errorResponse => {
        this.errors['Erro ao atualizar o cliente!']
      })

    }else {
    this.service
      .salvar(this.cliente)
      .subscribe( response => {
        this.success = true;
        this.errors = null;
      }, errorResponse => {
        this.success = false;
        this.errors = errorResponse.error.errors;
        })
    }
  }
}
