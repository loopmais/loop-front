export class Cliente {
    id: number;
    name: string;
    email: string;
    telefone: string;
    CPF: string;
    CEP: string;
    endereco: string;
    numero: number;
    bairro: string;
}