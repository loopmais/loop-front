import { ClientesSearchComponent } from './clientes-search/clientes-search.component';
import { ClientesListaComponent } from './clientes-lista/clientes-lista.component';
import { LayoutComponent } from './../layout/layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesFormComponent } from './clientes-form/clientes-form.component'

const routes: Routes = [
  { path: 'clientes', component: LayoutComponent, children: [
  {path: 'form', component: ClientesFormComponent},
  {path: 'form/:id', component: ClientesFormComponent},
  {path: 'lista', component: ClientesListaComponent},
  {path: 'search', component: ClientesSearchComponent},
    //desfazer esse que ficou apenas pra substituir o antigo
  {path: '', redirectTo: '/clientes/lista' || 'clientes/search', pathMatch: 'full'}
    
  ]}
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesRoutingModule { }
