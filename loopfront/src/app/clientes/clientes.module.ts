import { NgxMaskModule } from 'ngx-mask';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClientesRoutingModule } from './clientes-routing.module';
import { ClientesFormComponent } from './clientes-form/clientes-form.component';
import { ClientesListaComponent } from './clientes-lista/clientes-lista.component';
import { ClientesSearchComponent } from './clientes-search/clientes-search.component';


@NgModule({
  declarations: [ClientesFormComponent, ClientesListaComponent, ClientesSearchComponent],
  imports: [
    CommonModule,
    ClientesRoutingModule,
    FormsModule,
    NgxMaskModule.forChild(),
    ReactiveFormsModule
  ],
  exports: [
    ClientesFormComponent,
    ClientesListaComponent,
    ClientesSearchComponent
  ]
})
export class ClientesModule { }
