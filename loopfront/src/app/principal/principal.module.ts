import { PrincipalHomeComponent } from './principal-home/principal-home.component';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrincipalRoutingModule } from './principal-routing.module';


@NgModule({
  declarations: [PrincipalHomeComponent],
  imports: [
    CommonModule,
    PrincipalRoutingModule,
    FormsModule
  ], exports: [
    PrincipalHomeComponent
  ]
})
export class PrincipalModule { }
