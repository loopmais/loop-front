import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from '../../login/user';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  apiURL: string = environment.apiURLBase + '/api/register';

  constructor(
    private http: HttpClient
  ) { }
  registrar(cliente: User): Observable<any>{
    return this.http.post<any>(this.apiURL, User);
  }
}
