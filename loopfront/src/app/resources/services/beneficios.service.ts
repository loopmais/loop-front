import { Observable } from 'rxjs';
import { Beneficios } from '../../beneficios/beneficios';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BeneficiosService {

  apiURL: string = environment.apiURLBase + "/api/beneficios"

  constructor(private http: HttpClient) { }

  Salvar(beneficios: Beneficios): Observable<Beneficios>{
    return this.http.post<Beneficios>(this.apiURL, beneficios);
  }
}
