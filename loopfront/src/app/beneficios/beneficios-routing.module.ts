import { BeneficiosListaComponent } from './beneficios-lista/beneficios-lista.component';
import { LayoutComponent } from './../layout/layout.component';
import { BeneficiosFormComponent } from './beneficios-form/beneficios-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {path: 'beneficios/form', component: BeneficiosFormComponent},
    {path: 'beneficios/lista', component: BeneficiosListaComponent},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BeneficiosRoutingModule { }
